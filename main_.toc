\babel@toc {english}{}
\contentsline {section}{\numberline {1}Introduction and project motivations}{3}
\contentsline {section}{\numberline {2}Working with example}{3}
\contentsline {subsection}{\numberline {2.1}Problem view: combinatorial explosion of cell signalling systems}{3}
\contentsline {subsection}{\numberline {2.2}Yeast pheromone response pathway model}{4}
\contentsline {subsection}{\numberline {2.3}Rule-based modelling}{5}
\contentsline {subsection}{\numberline {2.4}Datasets and simulations}{8}
\contentsline {subsubsection}{\numberline {2.4.1}Perturbed model}{8}
\contentsline {subsubsection}{\numberline {2.4.2}Simulator}{9}
\contentsline {section}{\numberline {3}Applied methods and results}{9}
\contentsline {subsection}{\numberline {3.1}Correlation Explanation}{9}
\contentsline {subsubsection}{\numberline {3.1.1}Choice rationales}{9}
\contentsline {subsubsection}{\numberline {3.1.2}Method description}{11}
\contentsline {subsubsection}{\numberline {3.1.3}Results}{13}
\contentsline {subsubsection}{\numberline {3.1.4}Interpretation and analysis}{14}
\contentsline {subsection}{\numberline {3.2}Chaos Time Series Analysis}{16}
\contentsline {section}{\numberline {4}Conclusions}{25}
