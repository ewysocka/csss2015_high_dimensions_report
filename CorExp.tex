\documentclass[main.tex]{subfiles}
\begin{document}
\newcommand{\tc}{TC}
\newcommand{\argmax}{\arg\!\max}

In this section, we discuss an information theoretic approach for building a model on dynamics of the species concentrations. This method, proposed recently for a general domain~\cite{NIPS2014_5580,steeg2015corex_theory}, learns a hierarchy of latent variables that maximally inform correlation between the observed species dynamics. Herein, correlation refers to mutual information between a set of variables, and not just a linear correlation.

Before we delve into the details of the method for our specific settings, it should be noted that we disregard the time series nature of species copy-number dynamics in this method application.

Let $G$ be a set of random variables representing copy-numbers for all the species. Then, $X_G$ is a joint random variable on $G$. For a species $i$, all the copy-number values of the time series are assumed to be independent samples of a random variable $X_i$. As such, we can see that there is a contradiction since consecutive samples in the time series would have a correlation~(not i.i.d.). For obtaining uncorrelated samples, one can take sub-samples of the time series, either at uniform interval or using any other relevant technique.

Following the notations in \cite{NIPS2014_5580}, total correlation $\tc(.)$ between a set of variables $X_G$ is defined as below.

\begin{align}
&
\tc(X_G)
=
\sum_{i \in G}
H(X_i)
-
H(X_G)
\\
&
\tc(X_G)
=
I(X_1;\cdots;X_g)
\end{align}

Here $H(X_i)$ is entropy on a random variable $X_i$; and $H(X_G)$ is a joint entropy on $X_G$. Another interpretation of $\tc(X_G)$ is that it is mutual information, $I(.)$, between all the variables in the set $G$. Typically mutual information is expressed between a pair where each element of the pair can be a set of random variables. Here, we are instead expressing mutual information between a $g$ dimensional triplet of random variables, where $g$ is a number of random variables in the set $G$.

In our problem of learning a model of species dynamics, evaluating mutual information~(or total correlation) between all random variables would not be of much value. We are instead interested in evaluating mutual information between some subsets of species. However there are two problems along these lines: i) we do not know for which subsets of species we should evaluate mutual information and there can be a large number of permutations to explore~(depending on the size of a subset and the $G$ set); ii) non-parametric estimation of mutual information between random variables is a hard problem~\cite{kraskov2004estimating,suzuki2008approximating,pal2010estimation,shuyang2015MI}.
To tackle these, we formulate our algorithm such that; i) we assume the individual species copy-number variables $X_i$ to be Gaussian; however, we do not assume that \emph{the set of variables} has to be Gaussian~(the later is a stronger assumption); ii) we are interested in only those subsets where variables have low mutual information conditioning on a latent variable~(or high mutual information between variables explained by a latent variable).

Along these lines, let us define a new information theoretic quantity $\tc(X_G;Y_F)$.
\begin{align}
\tc(X_G;Y_F)
=
\tc(X_G)
-
\tc(X_G|Y_F)
\end{align}
$\tc(X_G;Y_F)$ is a total correlation~(or mutual information) in the set of random variables $X_G$ explained by a set of latent variables $Y_F$. $\tc(X_G|Y_F)$ is a total correlation between the random variables $X_G$ that can not be explained by $Y_F$, i.e. conditional total correlation~(conditional mutual information). If the latent variables $Y_F$ can explain the total correlation in $X_G$ perfectly, then $\tc(X_G|Y_F)=0$. Ideally, we would like to learn $Y_F$ if exists. Thus intuitively, optimal $Y$ would correspond to minimum of $\tc(X_G|Y_F)$. In our formulation, we can express optimization of $Y_F$ as optimizing conditional distributions $P_{Y|X}$.

Let us first consider optimization of a single latent variable $Y$, and then generalize it later.

\begin{align}
\argmax_{Y:p(y|x_G)}
\tc(X_G;Y)\ s.t.\ |Y|=k
\end{align}
Here $Y$ is a discrete random variable; $x_G$ is a sample of the random variable $X_G$ and $y$ is sample of $Y$. We optimize $Y$ by learning the conditional distribution $p(y|x_G)$. Now, we further extend it for multiple latent variables, where each latent variable explains total correlation in a subset of the species concentration variables.

\begin{align}
\argmax_{G_j, p(y_j|x_{G_j})}
\sum_{j=1}^m
\tc(X_{G_j}; Y_{j})
\end{align}

We have introduced $m$ latent variables here with $Y_j$ explaining correlation between random variables in the corresponding subset $G_j \subset G$. Here these subsets can have an overlap.

Optimizing the above objective function seems hard. However, as explained in detail in \cite{NIPS2014_5580,steeg2015corex_theory}, it can be solved very efficiently for practical purposes. We omit these optimization details and refer readers to the original papers introducing this algorithm for the first time~\cite{NIPS2014_5580,steeg2015corex_theory}. Computational complexity of the method is linear with respect to the number of samples and number of variables in the set $G$. Furthermore, as an unsupervised method, it requires no assumption about the learned model. The code implementation for this algorithm is publicly available from the original authors~\footnote{https://github.com/gregversteeg/CorEx}.
%\newcommand{\tc}{TC}
%\newcommand{\argmax}{\arg\!\max}

%\bibliographystyle{abbrv}
%\bibliography{references}
\end{document}
