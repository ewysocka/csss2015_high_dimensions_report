\select@language {english}
\contentsline {section}{\numberline {1}Introduction and project motivations}{5}
\contentsline {section}{\numberline {2}Working with example}{5}
\contentsline {subsection}{\numberline {2.1}Problem view: combinatorial explosion of cell signalling systems}{5}
\contentsline {subsection}{\numberline {2.2}Yeast pheromone response pathway model}{6}
\contentsline {subsection}{\numberline {2.3}Rule-based modelling}{7}
\contentsline {subsection}{\numberline {2.4}Datasets and simulations}{10}
\contentsline {paragraph}{\numberline {2.4.0.1}Perturbed model}{10}
\contentsline {paragraph}{\numberline {2.4.0.2}Simulator}{11}
\contentsline {section}{\numberline {3}Applied methods and results}{11}
\contentsline {subsection}{\numberline {3.1}Correlation Explanation}{11}
\contentsline {subsubsection}{\numberline {3.1.1}Choice rationales}{11}
\contentsline {subsubsection}{\numberline {3.1.2}Method description}{13}
\contentsline {subsubsection}{\numberline {3.1.3}Results}{15}
\contentsline {subsubsection}{\numberline {3.1.4}Interpretation and analysis}{18}
\contentsline {subsection}{\numberline {3.2}Chaos Time Series Analysis}{19}
\contentsline {section}{\numberline {4}Conclusions}{26}
