(TeX-add-style-hook
 "CorExp"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("subfiles" "main.tex")))
   (TeX-run-style-hooks
    "latex2e"
    "subfiles"
    "subfiles10")
   (TeX-add-symbols
    "tc"
    "argmax"))
 :latex)

