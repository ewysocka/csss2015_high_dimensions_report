(TeX-add-style-hook
 "main_"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "a4paper" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("babel" "english") ("appendix" "toc" "page")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "Motivations"
    "CombinatorialExplosion"
    "CorExp_motivations"
    "CorExp"
    "CorEx_results"
    "CorEx_interpretation"
    "TimeSeries"
    "Conclusions"
    "article"
    "art11"
    "url"
    "amsfonts"
    "graphicx"
    "inputenc"
    "fontenc"
    "babel"
    "indentfirst"
    "amsmath"
    "times"
    "sectsty"
    "listings"
    "subfiles"
    "setspace"
    "lipsum"
    "courier"
    "caption"
    "subcaption"
    "xcolor"
    "tocloft"
    "capt-of"
    "float"
    "appendix"
    "afterpage"
    "authblk")
   (TeX-add-symbols
    '("horrule" 1))
   (LaTeX-add-bibliographies
    "references"))
 :latex)

