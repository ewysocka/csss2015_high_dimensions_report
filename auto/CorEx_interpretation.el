(TeX-add-style-hook
 "CorEx_interpretation"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("subfiles" "main.tex")))
   (TeX-run-style-hooks
    "latex2e"
    "subfiles"
    "subfiles10")
   (LaTeX-add-labels
    "fig:biounpert1"
    "fig:biounpert2"
    "fig:biopert1"
    "fig:biopert2"
    "fig:obs2rule1"
    "fig:obs2rule2"
    "fig:fluxmap"
    "fig:fluxmapclust"))
 :latex)

